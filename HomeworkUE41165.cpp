﻿// HomeworkUE41165.cpp :
// Закрепить навык работы с массивами данных и навык преобразования типов данных.

#include "MyLittleHelpers.h" // мои вспомогательные функции
#include <iostream>
#include <time.h>  // необходимо для получения текущей даты
#include <iomanip> // необходимо для выравнивания таблицы


void JustLine(int NumCol) // функция для "рисования" таблицы
{
    std::cout << " +"; // первоначальный отступ у левого края
    for (int k = 0; k < NumCol; k ++)
    {
        std::cout << "----+";
    }
    std::cout << std::endl; // просто перенос строки
}

int main()
{
 
    ConsoleCP cp(1251); //переключаем ввод и вывод консоли на кириллицу
    
    int ArraySum = 0;        // переменная под сумму элементов строки массива
    const int N = 12;         // размерность массива
    int ArrayMatrix[N][N]{}; // объявляем массив без инициализации

    JustLine(N); // первая линия оформления таблицы
    
    for (int i = 0; i < N; i++)
    {
        std::cout << " | ";
        for (int j = 0; j < N; j++)
        {
            ArrayMatrix[i][j] = i + j; // инициализируем массив значениями i+j
            std::cout << std::setw(2) << ArrayMatrix[i][j] << " | "; // и сразу выводим значения массива ы консоль
        }
        std::cout << std::endl; // просто перенос строки
        JustLine(N); // оформление таблицы
    }

    // получаем текущую дату
    struct tm buf;
    time_t t = time(NULL);
    localtime_s(&buf, &t);

    // подсчет суммы строки массива
    for (int j = 0; j < N; j++)
    {
     // сумма эл-тов в строке массива, индекс которой равен остатку деления текущего числа календаря на N
        ArraySum += ArrayMatrix[(buf.tm_mday % N)][j];
    }
    std::cout << "[Текущая строка массива]---[";
    std::cout << std::setw(2) << (buf.tm_mday % N) << "]" << std::endl;
    std::cout << "[Сумма элементов строки]--[";
    std::cout << std::setw(3) << ArraySum << "]" << std::endl;

    return 0;
}
